<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
// 	$people = ['Person 1','Person 2','Person 4'];
// 	// return view('welcome')->withPeople($people);
// 	// return view('welcome')->with('people',$people);
//     return view('welcome', compact('people'));
//     // return view('welcome', ['People' => $people]);
// });

Auth::routes();

Route::get('admin_area', ['middleware' => 'admin', function () {
    //
	
}]);

// better not to add ->middleware('auth'); to the end of all these and instead define it in the respective controllers instead
// see: https://laracasts.com/series/laravel-5-from-scratch/episodes/14?autoplay=true

Route::get('/cards','CardsController@index')->middleware('auth');

// Route::get('/', 'PagesController@home')->middleware('auth');

Route::get('/about', 'PagesController@about')->middleware('auth');

Route::get('/cards/{card}','CardsController@show')->middleware('auth');

Route::post('/cards/{card}/notes','NotesController@store')->middleware('auth');

Route::get('/notes/{note}/edit','NotesController@edit')->middleware('auth');

Route::get('/redirect','PagesController@redirecttest')->middleware('auth');

Route::patch('/notes/{note}','NotesController@update')->middleware('auth');
// Without using a controller.. we can call a view directly
// Route::get('/about', function () {
//     return view('pages.about');
// });

Route::get('/', 'HomeController@index')->name('home');
