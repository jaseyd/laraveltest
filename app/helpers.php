<?PHP

function flash($message, $level = 'info') {
	// info, success, warning, danger (bootstrap alert classes)
	session()->flash('flash_message',$message);
	session()->flash('flash_message_level', $level);
}