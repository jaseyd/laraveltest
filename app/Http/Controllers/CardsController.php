<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Card;
use DB;

class CardsController extends Controller
{

	// works well
    // public function index() {
    // 	$cards = DB::table('cards')->get();
    // 	return view('cards.index',compact('cards'));
    // }

    public function index() {
    	$cards = Card::all();
    	return view('cards.index',compact('cards'));
    }


    public function show(Card $card) { // use typehinting to tell it which model we want to use
    	// $card = Card::find($id); // auto converts to JSON

        $card->load('notes.user');

    	// return $card; // auto converts to JSON
    	return view('cards.show',compact('card'));
    }
}
