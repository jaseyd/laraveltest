<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class PagesController extends Controller
{
    //
    public function home() {
		$people = ['Person 1','Person 2','Person 4'];
		// return view('welcome')->withPeople($people);
		// return view('welcome')->with('people',$people);
	    return view('welcome', compact('people'));
	    // return view('welcome', ['People' => $people]);
    }

    public function about() {
    	return view('pages.about');
    }

    public function redirecttest() {
        // Session::flash('status','redirect');
        // or
        // flash session.. exists for the next request only.. then is gone
        // session()->flash('status', 'Redirect was successful!');
        flash('You are now signed in!','success'); //info, success, warning, danger (Bootsteap alert classes)

        //persistant session.. stays until destroyed
        // session()->put('status', 'Redirect was successful!');
        return redirect('/');
        // or
        // return Redirect::to('/');
    }
}
