<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Card;
use App\Note;

class NotesController extends Controller
{
    public function store(Request $request, Card $card) {

    	$this->validate($request, [
    		'body' => 'required|min:10'
    	]);

    	//return $request->all();

    	// Way 1 // most manual
    	// $note = new Note;
    	// $note->body = $request->body;
    	// $card->notes()->save($note);

    	// way 2
  		// $note = new Note(['body' => $request->body]); // relies on fillable field
  		// $card->notes()->save($note);

		// way 3
		// $card->notes()->save(
		// 	new Note(['body' => $request->body])
		// );

		// way 4 // most obvious
		// $card->notes()->create([
		// 	'body' => $request->body
		// ]);

		// way 5
		// $card->notes()->create($request->all()); // security added by fillable field

		// way 6 // most readable.. requires a new function to be added to the Card object

    	$note = new Note($request->all());

    	// $note->user_id = 1;

    	$card->addNote($note, 1);

		return back();
    }

    public function edit(Note $note) {
    	return view('notes.edit', compact('note'));
    }

    public function update(Request $request, Note $note) {
    	$note->update($request->all());
    	return back();
    }
}
