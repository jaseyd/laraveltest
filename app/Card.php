<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
	public function notes() {
		// both the same
		// return $this->hasMany(Note::class);
		return $this->hasMany('App\Note');
	}

	public function addNote(Note $note, $userId) {
		$note->user_id = $userId;
		
		return $this->notes()->save($note);
	}

	// public function path() {
	// 	return 'cards/' . $this->id;
	// }
 //    //
}
