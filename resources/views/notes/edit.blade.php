@extends('layout')

@section('content')

	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<h1>Edit the Note</h1>

			<form method="POST" action="/laravel/test/public/notes/{{ $note->id }}">

				{{ method_field('PATCH') }}

				<div class="form-group">
					<textarea name="body" class="form-control">{{ $note->body }}</textarea>
				</div>

				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="form-group">
					<input type="submit" class="btn btn-primary" name="submit" value="Update Note">
				</div>
			</form>

		</div>
	</div>
@endsection

