@extends('layout')

@section('content')
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<h1>{{ $card->title }}</h1>

			<ul class="list-group">
				@foreach ($card->notes as $note)
					<li class="list-group-item">
						<a href='/laravel/test/public/notes/{{ $note->id }}/edit'>{{ $note->body }}</a>
						<a style='float:right;' href='#'>{{ $note->user->username }}</a>
					</li>
				@endforeach
			</ul>

			<hr>

			<h3>Add a New Note</h3>
			
			<form method="POST" action="{{ $card->id }}/notes">
				<div class="form-group">
					<textarea name="body" class="form-control">{{ old('body') }}</textarea>
				</div>

				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="form-group">
					<input type="submit" class="btn btn-primary" name="submit" value="Add Note">
				</div>
			</form>
			@if(count($errors))
				@foreach($errors->all() as $error)
					<h2>{{ $error }}</h2>
				@endforeach 
			@endif
		</div>
	</div>
@endsection